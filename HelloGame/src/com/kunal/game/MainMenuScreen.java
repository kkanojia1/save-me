package com.kunal.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class MainMenuScreen implements Screen {
	
	final Drop game;

	OrthographicCamera camera;
	
	Texture buttonImage;
	Rectangle button;
	private SpriteBatch batch;
	private Sprite sprite;

	public MainMenuScreen(final Drop gam) {
		game = gam;

		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);
		
		buttonImage =  new Texture(Gdx.files.internal("data/bucket.png"));
		
		sprite = new Sprite(buttonImage);

		// create a Rectangle to logically represent the bucket
		batch = new SpriteBatch();

		button = new Rectangle();
		button.x = 800 / 2 - 64 / 2;
		button.y = (480 / 2) - (64 / 2);
		button.width = 64;
		button.height = 64;

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		
		sprite.setPosition(button.x, button.y);
		
		sprite.draw(batch);
		
		game.font.draw(batch, "Welcome to Drop!!! ", 100, 150);
		game.font.draw(batch, "Tap anywhere to begin!", 100, 100);
		batch.end();

		if (Gdx.input.isTouched()) {
			game.setScreen(new GameScreen(game));
			dispose();
		}
		
		
	}


	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
